%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%
%
%	Toute la dimension graphique du CV
%
%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%

\ProvidesClass{etcresume}

\LoadClass[11pt,a4paper]{article}

\RequirePackage[scale=0.82]{geometry}
\RequirePackage{titlesec}
\RequirePackage{graphicx}
\RequirePackage[dvipsnames]{xcolor}
\RequirePackage{hyperref}
\RequirePackage{tabularx}
\RequirePackage{ifthen}
\RequirePackage{etoolbox}
\RequirePackage{fontspec}
\RequirePackage{fontawesome}

% Fontes
% =======================================================================

\setmainfont[
	Path=fonts/Barlow/,
	Extension=.ttf,
	UprightFont=*-Regular,
	BoldFont=*-SemiBold,
	ItalicFont=*-Italic,
	BoldItalicFont=*-SemiBoldItalic,
]{Barlow}

% Description du profil
% =======================================================================

\newcommand*{\name}[2]{\def\@firstname{#1}\def\@lastname{#2}}
\newcommand*{\jobtitle}[1]{\def\@jobtitle{#1}}
\newcommand*{\phone}[1]{\def\@phone{#1}}
\newcommand*{\photo}[2][80pt]{\def\@photowidth{#1}\def\@photopath{#2}}
\newcommand*{\email}[1]{\def\@email{#1}}
\newcommand*{\linkedin}[1]{\def\@linkedin{#1}}

% Caractéristiques du PDF
% =======================================================================

\AtEndPreamble{ % Pour que les variables aient bien été définies par l'utilisateur
\hypersetup{
	pdfauthor={\@firstname~\@lastname},
	pdftitle={CV},
	pdfsubject={Parcours professionnel},
	pdfkeywords={\@jobtitle},
	pdfcreator={\TeX},
	hidelinks
}
}

% Apparence
% =======================================================================

\pagenumbering{gobble} % Suppression des numéros de pages (c'est un CV, pas un bouquin)
\setlength{\tabcolsep}{0pt} % Espace entre les colonnes des tableaux

\titleformat{\section}{\normalfont\Large\color{Orange}}{}{0em}{}[{\titlerule[0.6pt]}]
\titlespacing*{\section}{0pt}{8pt}{8pt}

\newcommand{\tabwidth}{.14\textwidth}

% Éléments du CV
% =======================================================================

% Une compétence
% label, activité
\newcommand{\cvskill}[2]{
	\setlength\topsep{1pt}
	\begin{flushleft}
	\begin{tabularx}{\textwidth}{p{\tabwidth}X}
	#1 & #2
	\end{tabularx}
	\end{flushleft}
}

% Un événement (formation, poste…)
% label, titre, structure, domaine, [description]
\newcommand{\cvstep}[5]{
	\setlength\topsep{1pt}
	\begin{flushleft}
	\begin{tabularx}{\textwidth}{p{\tabwidth}X}
	#1 &
	\textbf{#2}%
	\ifthenelse{\equal{#3}{}}{}{, \textit{#3}}%
	\ifthenelse{\equal{#4}{}}{}{, #4}%
	\ifthenelse{\equal{#5}{}}{}{\\ & \small #5}%
	\end{tabularx}
	\end{flushleft}
}

% L'en-tête
% Défini à partir des variables de profil (métier, nom…)
\newcommand{\cvheader}{
	% Coordonnées personnelles
	\begin{flushleft}
	\begin{minipage}[b]{0.7\textwidth}
	\raggedright
	\color{gray}
	\Large
	\@firstname~\textsc{\@lastname}\\
	\medskip
	\normalsize
	\faPhoneSquare~\@phone\\
	\faEnvelopeSquare~\href{mailto:\@email}{\it \@email}\\
	\ifthenelse{\isundefined{\@linkedin}}{}{\faLinkedinSquare~\href{https://www.linkedin.com/in/\@linkedin/}{\it \@linkedin}}
	\end{minipage}%
	\begin{minipage}[b]{0.3\textwidth}
	\raggedleft
	\includegraphics[width=\@photowidth]{\@photopath}
	\end{minipage}

	% Titre
	\begin{center}
	\Huge
	\@jobtitle
	\end{center}
	\end{flushleft}
}
