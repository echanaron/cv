build:
	lualatex cv.tex

all:
	make && make

clean:
	@rm -f cv.aux cv.log cv.out
